Sample configuration files for:

SystemD: schillingcoind.service
Upstart: schillingcoind.conf
OpenRC:  schillingcoind.openrc
         schillingcoind.openrcconf
CentOS:  schillingcoind.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
